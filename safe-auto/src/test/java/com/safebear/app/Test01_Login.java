package com.safebear.app;

import com.safebear.app.BaseTest;
import com.safebear.app.pages.WelcomePage;
import org.junit.Test;

import static junit.framework.TestCase.assertTrue;

/**
 * Created by CCA_Student on 24/10/2017.
 */
public class Test01_Login extends BaseTest {

    @Test
    public void testLogin() {
        //Step 1 Confirm we're on the Welcome page
        assertTrue(welcomePage.checkCorrectPage());

        //Step 2 click on the Login link and the Login Page loads
        assertTrue(welcomePage.clickOnLogin(this.loginPage));

        //Step 3 Login with valid credentials
        utility.importCSV();
        assertTrue(loginPage.login(this.userPage, utility.getUserName(), utility.getPassword()));

        //Step 4 Logout

    }



}

package com.safebear.app.utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by CCA_Student on 24/10/2017.
 */
public class Utils {

    private WebDriver driver = new ChromeDriver();

    //DesiredCapabilities capabilities = DesiredCapabilities.htmlUnit();
    //private WebDriver driver = new RemoteWebDriver(new URL("http://127.0.0.1:4444/wd/hub"), capabilities);
    private String url = "http://automate.safebear.co.uk/";
    private long timeOut = 4;

    private String userName;// = "testuser";
    private String password;// = "testing";

    // exception will be passed on further in the chain. In this case its the BaseTest
    //public Utils() throws MalformedURLException {
   // }

    //private String csvFile = "/Libraries/users.csv";

    public String getUserName() {
        return userName;
    }

    public String getPassword() {
        return password;
    }

    public long getTimeOut() {
        return timeOut;
    }

    public WebDriver getDriver() {
        return driver;
    }

    public String getUrl() {
        return url;
    }

    //import csv

    public void importCSV() {

        // I changed the 'data' folder so it's under the test folder and marked as 'Test Resources
        // root' (right click the folder and choose 'Mark Directory As...')
        String csvFile = "users.csv";
        String cvsSplitBy = ",";

        // I'm now using an ArrayList to record the lines in the file
        ArrayList<String> result = getFile(csvFile);

        // Simple 'for' loop to read through the contents of the ArrayList
        for (String userLine : result) {

            // Split each line by commas - same as before
            String[] user = userLine.split(cvsSplitBy);
            this.userName = user[0];
            this.password = user[1];
            System.out.println("User [name = " + user[0] + " , password= " + user[1] + "]");

        }
    }

    // This is where we get the file and return the ArrayList
    private ArrayList<String> getFile(String fileName){

        // Create the ArrayList
        ArrayList<String> result = new ArrayList<String>();

        // Open up the file using ClassLoader
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource(fileName).getFile());

        // Populate ArrayList with the lines of the file
        try (Scanner scanner = new Scanner(file)) {
            while (scanner.hasNextLine()){
                   result.add(scanner.nextLine());
            }
            scanner.close();
        } catch (IOException e){
            e.printStackTrace();
        }

        // Return the array
        return result;

    }

}

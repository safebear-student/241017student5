package com.safebear.app;

import com.safebear.app.pages.LoginPage;
import com.safebear.app.pages.UserPage;
import com.safebear.app.pages.WelcomePage;
import com.safebear.app.utils.Utils;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

import static junit.framework.TestCase.assertTrue;

/**
 * Created by RAS on 24/10/2017 first test.
 * Includes BaseTest and tearDown
 */
public class BaseTest {

    WebDriver driver;
    Utils utility;
    WelcomePage welcomePage;
    LoginPage loginPage;
    UserPage userPage;

    @Before
    public void setUp() {

        utility = new Utils();
        //try {
           // utility = new Utils();
        //} catch (MalformedURLException e) {
           // e.printStackTrace();
           // System.out.println("Your headless URL is rubbish, please sort it out!");
       // }
        this.driver = utility.getDriver();

        //instantiate the welcome and login pages
        welcomePage = new WelcomePage(driver);
        loginPage = new LoginPage(driver);
        userPage = new UserPage(driver);

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        // Go to the URL
        driver.get(utility.getUrl());
        // maximise window
        driver.manage().window().maximize();

    }

    @After
    public void tearDown() {
        try {
            TimeUnit.SECONDS.sleep(utility.getTimeOut());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.quit();
    }

}
